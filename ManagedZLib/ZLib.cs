﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManagedZLib_native;
using System.Runtime.InteropServices;

namespace ManagedZLib
{
    public static class ZLib
    {
        public enum CompressionLevels
        {
            NO_COMPRESSION = 0,
            BEST_SPEED = 1,
            BEST_COMPRESSION = 9,
            DEFAULT_COMPRESSION = (-1)
        };

        public static byte[] Compress(byte[] source, CompressionLevels level = CompressionLevels.DEFAULT_COMPRESSION)
        {
            uint[] destSize = new uint[] { ZLibNative.CompressBound((uint)source.Length) };
            byte[] dest = new byte[destSize[0]];

            GCHandle dstSizeHandle = GCHandle.Alloc(destSize, GCHandleType.Pinned);
            GCHandle dstHandle = GCHandle.Alloc(dest, GCHandleType.Pinned);
            GCHandle srcHandle = GCHandle.Alloc(source, GCHandleType.Pinned);

            ZLibNative.Compress(dstHandle.AddrOfPinnedObject(), dstSizeHandle.AddrOfPinnedObject(), srcHandle.AddrOfPinnedObject(), (uint)source.Length, (ZLibCompressionLevels)level);

            byte[] result = new byte[destSize[0]];
            for (uint i = 0; i < result.Length; i++)
            {
                result[i] = dest[i];
            }

            srcHandle.Free();
            dstHandle.Free();
            dstSizeHandle.Free();

            return result;
        }

        public static byte[] Uncompress(byte[] source, int uncompressedSize)
        {
            byte[] dest = new byte[uncompressedSize];
            uint[] destSize = new uint[] { (uint)uncompressedSize };

            GCHandle destSizeHandle = GCHandle.Alloc(destSize, GCHandleType.Pinned);
            GCHandle destHandle = GCHandle.Alloc(dest, GCHandleType.Pinned);
            GCHandle srcHandle = GCHandle.Alloc(source, GCHandleType.Pinned);

            ZLibNative.Uncompress(destHandle.AddrOfPinnedObject(), destSizeHandle.AddrOfPinnedObject(), srcHandle.AddrOfPinnedObject(), (uint)source.Length);

            srcHandle.Free();
            destHandle.Free();
            destSizeHandle.Free();

            return dest;
        }
    }
}
