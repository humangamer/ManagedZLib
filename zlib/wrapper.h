#ifndef _WRAPPER_H_
#define _WRAPPER_H_
#include <zlib.h>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace ManagedZLib_native
{
	public enum class ZLibCompressionLevels
	{
		NO_COMPRESSION = 0,
		BEST_SPEED = 1,
		BEST_COMPRESSION = 9,
		DEFAULT_COMPRESSION = (-1)
	};

	public ref class ZLibNative
	{
		public:
			static int Compress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen);
			static int Compress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen, ZLibCompressionLevels level);
			//static uLongf Compress(IntPtr dest, IntPtr source, uLong sourceLen, ZLibCompressionLevels level);
			static uLong CompressBound(uLong sourceLen);
			static int Uncompress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen);
	};
}

#endif // _WRAPPER_H_
