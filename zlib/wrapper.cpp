#include "wrapper.h"


namespace ManagedZLib_native
{
	int ZLibNative::Compress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen)
	{
		return ::compress(reinterpret_cast<::Bytef*>(dest.ToPointer()), reinterpret_cast<::uLongf*>(destLen.ToPointer()), reinterpret_cast<::Bytef*>(source.ToPointer()), sourceLen);
	}

	int ZLibNative::Compress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen, ZLibCompressionLevels level)
	{
		return ::compress2(reinterpret_cast<::Bytef*>(dest.ToPointer()), reinterpret_cast<::uLongf*>(destLen.ToPointer()), reinterpret_cast<::Bytef*>(source.ToPointer()), sourceLen, (int)level);
	}

	/*uLongf ZLibNative::Compress(IntPtr dest, IntPtr source, uLong sourceLen, ZLibCompressionLevels level)
	{
		uLongf destLen;
		::compress2(reinterpret_cast<::Bytef*>(dest.ToPointer()), &destLen, reinterpret_cast<::Bytef*>(source.ToPointer()), sourceLen, (int)level);
		return destLen;
	}*/
	
	uLong ZLibNative::CompressBound(uLong sourceLen)
	{
		return ::compressBound(sourceLen);
	}

	int ZLibNative::Uncompress(IntPtr dest, IntPtr destLen, IntPtr source, uLong sourceLen)
	{
		return ::uncompress(reinterpret_cast<::Bytef*>(dest.ToPointer()), reinterpret_cast<::uLongf*>(destLen.ToPointer()), reinterpret_cast<::Bytef*>(source.ToPointer()), sourceLen);
	}
}